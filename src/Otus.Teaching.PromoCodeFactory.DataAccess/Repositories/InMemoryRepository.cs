﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IList<T> Data { get; set; }

        public InMemoryRepository(IList<T> data)
        {
            Data = data;
        }

        public Task CreateAsync(T entity)
        {
            Data.Add(entity);
            return Task.CompletedTask;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task UpdateAsync(T entity)
        {
            var employee = Data.FirstOrDefault(item => item.Id == entity.Id);
            Data[Data.IndexOf(employee)] = entity;
            return Task.CompletedTask;
        }

        public Task DeleteAsync(Guid id)
        {
            Data.Remove(Data.FirstOrDefault(item => item.Id == id));
            return Task.CompletedTask;
        }
    }
}